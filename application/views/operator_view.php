<!-- NAVIGATION : This navigation is also responsive-->
			<nav>
				<ul>
					<!--li>
						<a href="index.html" title="Dashboard"><i class="fa fa-lg fa-fw fa-home"></i> <span class="menu-item-parent">Dashboard</span></a>
					</li-->
					
					<li>
						<a href="<?php echo base_url();?>admin/user" title="User"><i class="fa fa-lg fa-fw fa-user"></i> <span class="menu-item-parent">User</span></a>
					</li>
					
					<li class="active">
						<a href="<?php echo base_url();?>admin/operator" title="Operator"><i class="fa fa-lg fa-fw fa-cogs"></i> <span class="menu-item-parent">Operator</span></a>
					</li>
										 
					<!--li>
						<a href="#"><i class="fa fa-lg fa-fw fa-table"></i> <span class="menu-item-parent">Demo</span></a>
						<ul>
							<li>
								<a href="#">Demo1</a>
							</li>
							<li>
								<a href="#">Demo1</a>
							</li>
							<li>
								<a href="#">Demo2</a>
							</li>
						</ul>
					</li-->
					  
				</ul>
			</nav>
			<span class="minifyme" data-action="minifyMenu"> 
				<i class="fa fa-arrow-circle-left hit"></i> 
			</span>

		</aside>
		<!-- END NAVIGATION -->

		<!-- MAIN PANEL -->
		<div id="main" role="main">

		<!-- MAIN CONTENT -->
			<div id="content">

			
				<!-- widget grid -->
				<section id="widget-grid" class="">
				
					<!-- row -->
					<div class="row">
				
						<!-- NEW WIDGET START -->
						<article class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
				
							<!-- Widget ID (each widget will need unique ID)-->
							
							<!-- end widget -->
				
							<!-- Widget ID (each widget will need unique ID)-->
							
							<button type="button" id="addOperatorValid" class="btn btn-default add"  data-toggle="modal" data-target="#myModal">Add Operator</button>
							
							<?php if($this->session->flashdata('successmsg')!=''){ ?>
									<div class="alert alert-success text-center" id="successmsg" >
									  <?php echo $this->session->flashdata('successmsg'); ?>
									  </div>
							<?php } ?>
								
							<div class="jarviswidget jarviswidget-color-blueDark" id="wid-id-1" data-widget-sortable="false" data-widget-collapsed="false"  data-widget-editbutton="false" data-widget-editbutton="false" data-widget-colorbutton="false" data-widget-deletebutton="false" data-widget-fullscreenbutton="false">
								
								<header>
									<span class="widget-icon"> <i class="fa fa-table"></i> </span>
									<h2>Operator List </h2>
				
								</header>
				
								<!-- widget div-->
								<div>
				
									<!-- widget edit box -->
								
									<!-- end widget edit box -->
				
									<!-- widget content -->
									
									
								
									
									<div class="widget-body no-padding">
										<table id="datatable_tabletools" class="table table-striped table-bordered table-hover" width="100%">
											<thead>
												<tr>
													<th data-hide="phone">ID</th>
													<th data-class="expand">Operator Name</th>
													<th>Country</th>
													<th data-hide="phone">Primary Contact</th>
													<th data-hide="phone,tablet">Email</th>
													<th data-hide="phone,tablet">Status</th>
													<th data-hide="phone,tablet">Action</th>
													
												</tr>
											</thead>
											<tbody>
											<?php $i=1; foreach($arrOperatorList as $arrResult){  ?>	
												<tr>
													<td><?php echo $i; ?></td>
													<td><?php echo $arrResult['operatorname']; ?></td>
													<td><?php echo $arrResult['country_name']; ?></td>
													<td><?php echo $arrResult['contactname']; ?></td>
													<td><?php echo $arrResult['email']; ?></td>
													<td><?php if($arrResult['status'] == '1'){ echo 'Active'; }else{ echo 'Inactive'; } ?></td>
													<td>
													<?php //if($arrResult['status'] == '1'){ ?>
													<a href="#" onclick="editOperator('<?php echo $arrResult['id']; ?>');" data-toggle="modal" data-target="#myEditOperatorModal"><i class="fa fa-edit"></i> Edit</a>&nbsp;<?php if($arrResult['status'] == '1'){ ?>|&nbsp;<a href="#" onclick="deleteOperator('<?php echo $arrResult['id']; ?>');"><i class="fa fa-times"></i> Delete</a><?php } ?> 
													<?php //} ?>
													</td>													
												</tr>
											<?php $i++; } ?>	
											</tbody>
										</table>
									
									</div>
									<!-- end widget content -->
				
								</div>
								<!-- end widget div -->
				
							</div>
							<!-- end widget -->
							
				<!-- modal-->
				 <div class="modal fade" id="myModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
					<div class="modal-dialog">
						<div class="modal-content">
							<div class="modal-header">
								<button type="button" class="close" data-dismiss="modal" aria-hidden="true">
									&times;
								</button>
								<h4 class="modal-title" id="myModalLabel">New Operator Form</h4>
							</div>
							
						 <div class="modal-body">
			
							   <form name="addOperatorform" class="form-horizontal" id="addOperatorform" method="post" action="<?php echo base_url();?>admin/operator/addOperator">	
								 
									<div class="form-group">
											<label for="operatorName" class="control-label col-md-3">Operator Name</label>
											<div class="col-md-6">
											<input type="text" class="form-control" id="operatorName" name="operatorName" placeholder="Operator Name" maxlength="25" autocomplete="off" />
										</div>
									</div>									
								
								
						
									 <div class="form-group">												
												<label for="country" class="control-label col-md-3">Country</label>
												<div class="col-md-6">
													<select class="form-control" id="country" name="country">
														<option value="">Select Country</option>
														<?php  foreach($arrCountryList as $key=>$value){ ?>
														<option  value="<?php echo $value['countrycode'].'~'.$value['countryname']; ?>"><?php echo $value['countryname']; ?></option>      
														<?php } ?>	
													</select>
											   </div>
								        </div>
							
								 
								
									
										<div class="form-group">											
											<label for="oper_email" class="col-md-3 control-label">Email Address</label>
											<div class="col-md-6">
											<input type="text" class="form-control" name="oper_email" id="oper_email" placeholder="Enter email address" maxlength="40" autocomplete="off" />
										   </div>
									</div>
								
									
							
									
										<div class="form-group">
											<label for="contactName" class="col-md-3 control-label">Primary Contact</label>
									   <div class="col-md-6">
											<input type="text" class="form-control" name="contactName" id="contactName" placeholder="Primary Contact Name" maxlength="30" autocomplete="off" />
										</div>
									</div>
							 
								<!--div class="row">
									<div class="col-md-6">
										<div class="form-group">
											<label for="password">Enter password</label>
											<input type="password" class="form-control" name="password" id="password" placeholder="Enter Password" maxlength="25" autocomplete="off" />
										</div>
									</div>
								</div-->
				                </div>
						
							<div class="modal-footer text-center">
								<!--button type="button" class="btn btn-default" >
									Ok
								</button-->
								<input type="submit" name="addOperator" id="addOperator" value="Ok" class="btn btn-default">
								
								<button type="button" class="btn btn-primary" data-dismiss="modal">
									cancel
								</button>
							</div>
							</form>
						</div><!-- /.modal-content -->
					</div><!-- /.modal-dialog -->
				</div>
						

	<!-- modal end-->


	<!-- modal Edit Operator-->
				 <div class="modal fade" id="myEditOperatorModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
					<div class="modal-dialog">
					<form name="editOperatorform"  id="editOperatorform" class="form-horizontal" method="post" action="<?php echo base_url();?>admin/operator/updateOperator">
						<div class="modal-content">
							<div class="modal-header">
								<button type="button" class="close" data-dismiss="modal" aria-hidden="true">
									&times;
								</button>
								<h4 class="modal-title" id="myModalLabel">Edit Operator Form</h4>
							</div>
							<div class="modal-body">
						
								<input type="hidden" name="editOperatorId" id="editOperatorId" value="" />
							
										<div class="form-group">
											<label for="editOperatorName" class="control-label col-md-3" >Operator Name</label>
											<div class="col-md-6">
											<input type="text" class="form-control" id="editOperatorName" name="editOperatorName" placeholder="Operator Name" maxlength="25" autocomplete="off" />
										</div>
									</div>									
								
							
									
											<div class="form-group">												
												<label for="editCountry" class="control-label col-md-3" >Country</label>
											<div class="col-md-6">
												<select class="form-control" id="editCountry" name="editCountry">
													<option value="">Select Country</option>
													<?php  foreach($arrCountryList as $key=>$value){ ?>
													<option  value="<?php echo $value['countrycode'].'~'.$value['countryname']; ?>"><?php echo $value['countryname']; ?></option>      
													<?php } ?>	
												</select>
											</div>
										</div>
								
									
										<div class="form-group">											
											<label for="editOper_email" class="control-label col-md-3" >Email Address</label>
										<div class="col-md-6">
											<input type="text" class="form-control" name="editOper_email" id="editOper_email" placeholder="Enter email address" maxlength="40" autocomplete="off" />
										</div>
									</div>
								
																
									
										<div class="form-group">
											<label for="editContactName" class="control-label col-md-3" >Primary Contact</label>
										<div class="col-md-6">
											<input type="text" class="form-control" name="editContactName" id="editContactName" placeholder="Primary Contact Name" maxlength="30" autocomplete="off" />
										</div>
									</div>
									
								
								
									
									<div class="form-group"  id="status" style="display:none">												
										<label for="editStatus" class="control-label col-md-3" >Status</label>
											<div class="col-md-6">
												<select class="form-control" id="editStatus" name="editStatus">
													<option value="">Select Status</option>
													<option value="0">Inactive</option>
													<option value="1">Active</option>
												</select>
											</div>
										</div>
										
										</div>
								
								<!--div class="row">
									<div class="col-md-6">
										<div class="form-group">
											<label for="password">Enter password</label>
											<input type="password" class="form-control" name="password" id="password" placeholder="Enter Password" maxlength="25" autocomplete="off" />
										</div>
									</div>
								</div-->
				
						
							<div class="modal-footer text-center">
								<!--button type="button" class="btn btn-default" >
									Ok
								</button-->
								<input type="submit" name="editOperator" id="editOperator" value="Ok" class="btn btn-default">
								
								<button type="button" class="btn btn-primary" data-dismiss="modal">
									cancel
								</button>
							</div>
								</div>
							</form>
						</div><!-- /.modal-content -->
					</div><!-- /.modal-dialog -->
				</div>
						

	<!-- modal end-->	

	
						</article>
						<!-- WIDGET END -->
				
					</div>
				
					<!-- end row -->
				
					<!-- end row -->
				
				</section>
				<!-- end widget grid -->

			</div>
			<!-- END MAIN CONTENT -->

		</div>
		<!-- END MAIN PANEL -->
		<script>
 
  $(document).ready(function() {
	//alert('hai');
	
	// validate the comment form when it is submitted

		// validate signup form on keyup and submit
		$("#addOperatorform").validate({
			rules: {
				operatorName: "required",
				country: "required",
				oper_email: {
					required: true,
					email: true
				},
				contactName: "required",	
				//password: "required",
				
				//building_name: "required",
				//street: "required",
				//city: "required",
				/*houseNo: {
					required: true,
					number: true
				},				
				*/
				
			},
			messages: {
				operatorName: "Please enter a operator name",
				country: "Please select a country",
				oper_email: {
					required: "Please enter an email address",
					email: "Please enter valid email id."
				},
				contactName: "Please enter a contact name",
				//password: "Please enter a password",
				
				//building_name: "Please select a house name",
				//street: "Please select a street name",
				//city: "Please select a town name",
				/*houseNo: {
					required: "Please enter a house number",
					number: "Please enter valid house number.",
				},				
				*/					
			}
		});	

		
		$("#editOperatorform").validate({
			rules: {
				editOperatorName: "required",
				editCountry: "required",
				editOper_email: {
					required: true,
					email: true
				},
				editContactName: "required",
				editStatus: "required",	
				//password: "required",
				
				//building_name: "required",
				//street: "required",
				//city: "required",
				/*houseNo: {
					required: true,
					number: true
				},				
				*/
				
			},
			messages: {
				editOperatorName: "Please enter a operator name",
				editCountry: "Please select a country",
				editOper_email: {
					required: "Please enter an email address",
					email: "Please enter valid email id."
				},
				editContactName: "Please enter a contact name",
				editStatus: "Please select a status",
				//password: "Please enter a password",
				
				//building_name: "Please select a house name",
				//street: "Please select a street name",
				//city: "Please select a town name",
				/*houseNo: {
					required: "Please enter a house number",
					number: "Please enter valid house number.",
				},				
				*/					
			}
		});	 
			
  });
  
  
  $("#operatorName").blur(function(){
			$("#operatorName").valid();
 	});	
	$("#country").blur(function(){
			$("#country").valid();
 	});
	$("#oper_email").blur(function(){
			$("#oper_email").valid();
 	});		
	$("#contactName").blur(function(){
			$("#contactName").valid();
 	});
	/*$("#password").blur(function(){
			$("#password").valid();
 	});*/
	
	$("#addOperatorValid").click(function(){
		$("em").empty();
	});
	
	
	$("#editOperatorName").blur(function(){
			$("#editOperatorName").valid();
 	});	
	$("#editCountry").blur(function(){
			$("#editCountry").valid();
 	});
	$("#editOper_email").blur(function(){
			$("#editOper_email").valid();
 	});		
	$("#editContactName").blur(function(){
			$("#editContactName").valid();
 	});
	/*$("#password").blur(function(){
			$("#password").valid();
 	});*/
	
	function editOperator(operatorId){
		//alert(operatorId);
		$("em").empty();	
		var dataString = 'operatorId='+operatorId;		
		 $.ajax({
			   type: "POST",
			   url: "<?php echo base_url(); ?>admin/operator/getOperatorById",
			   data: dataString,
			   dataType: 'json',
			   success: function(data){ 
					//alert(data);
					//alert(data.OperatorId);
					$("#editOperatorId").val(data.OperatorId);
					$("#editOperatorName").val(data.OperatorName);
					$("#editCountry").val(data.Country_code+'~'+data.Country_Name);
					$("#editOper_email").val(data.Email);
					$("#editContactName").val(data.ContactName);
					$("#editStatus").val(data.Status);
					//alert(data.Status);
					if(data.Status=='' || data.Status=='0'){
						$("#status").show();
					}else{
						$("#status").hide();
					}	
			   }
			 }); 
	}


  function deleteOperator(operatorId) {
   
  $.SmartMessageBox({
    title: "Confirmation alert?",
    content: "Are you want to delete this Operator?",
    buttons: '[No][Yes]'
  },
  function(ButtonPressed) {
    if (ButtonPressed === "Yes") {

     window.location.href = "<?php echo base_url(); ?>admin/operator/deleteOperatorById/"+operatorId;
    }
   
  });
  operatorId.preventDefault();
}
		
	  
</script>