		<!-- MAIN PANEL -->
		<div id="main" role="main">

		<!-- MAIN CONTENT -->
			<div id="content">

				<div class="row">
					<div class="col-xs-12 col-sm-7 col-md-7 col-lg-4">
						<h3 class="page-title txt-color-blueDark"><i class="fa-fw fa fa-hand-o-right"></i> New Subscriptions </h3>
						<!-- <span>&nbsp;>&nbsp; Device List</span> -->
					</div> 
				</div>
				<!-- widget grid -->
				<section id="widget-grid" class="">
				
					<!-- row -->
					<div class="row">
				
						<!-- NEW WIDGET START -->
						<article class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
				
							<!-- Widget ID (each widget will need unique ID)-->
							
							<!-- end widget -->
				
							<!-- Widget ID (each widget will need unique ID)-->
							<button type="button" class="btn btn-default add"  data-toggle="modal" data-target="#myModal">Add User</button>					
							<div class="jarviswidget jarviswidget-color-blueDark m-t-20" id="wid-id-1" data-widget-editbutton="false" data-widget-editbutton="false"  data-widget-togglebutton="false" data-widget-editbutton="false" data-widget-colorbutton="false" data-widget-deletebutton="false" data-widget-fullscreenbutton="false" >
								
								<header>
									<span class="widget-icon"> <i class="fa fa-table"></i> </span>
									<h2>User List</h2>				
								</header>
				
								<!-- widget div-->
								<div>
				
									<!-- widget edit box -->
								
									<!-- end widget edit box -->
				
									<!-- widget content -->
									<div class="widget-body no-padding">
										
										<table id="datatable_tablemodal" class="table table-striped table-bordered table-hover" width="100%">
											<thead>
												<tr>
													<th data-class="expand">Account ID</th>
													<th data-hide="phone">Subscription ID</th>
													<th>Mundio Number</th>
													<th data-hide="phone">Etisalat Number</th>
													<th data-hide="phone,phoneL,tablet,tabletL">Date of Allocation</th>
													<th data-hide="phone,phoneL,tablet,tabletL">Status</th>
													<th data-hide="phone,phoneL,tablet,tabletL,desktop">Date of Activation</th>
													<th data-hide="phone,phoneL,tablet">Reason if Rejected</th>
													<th data-hide="phone,phoneL,tablet,desktop">Customer Name</th>
													<th data-hide="phone,phoneL,tablet,tabletL,desktop,desktopL">Address Details</th>	
													<th data-hide="phone,phoneL,tablet,tabletL,desktop,desktopL,desktopXL">Alternate Phone Number</th>
													<th data-hide="phone,phoneL,tablet,tabletL,desktop,desktopL,desktopXL">SIM Collected</th>
													<th data-hide="phone,phoneL,tablet,tabletL,desktop,desktopXL">IP Address</th>
													<th data-hide="phone,phoneL,tablet,tabletL">Login Name</th>
													<th data-hide="phone,phoneL,tablet">Email</th>
													<th class="width200">Action</th>
											    </tr>
											</thead>
											<tbody>
											<?php //$i=1; foreach($arrOperatorUserList as $arrResult){  ?>
											<?php for($i=25;$i>=0;$i--){  ?>
												<tr>
													<td>1</td>
													<td>test</td>
													<td>test</td>
													<td>test</td>
													<td>test</td>
													<td>test</td>
													<td>test</td>													
												 	<td>test</td>
													<td>test</td>
													<td>test</td>
													<td>test</td>
													<td>test</td>
													<td>test</td>
													<td>test</td>
													<td>test</td>
													<td class="width200"><a href="#" data-toggle="modal" data-target="#myAcceptModal"><i class="fa fa-edit"></i> Accept</a>
													<a href="#" data-toggle="modal" data-target="#myRejectModal"><i class="fa fa-times"></i> Reject</a></td>
												</tr>	
											<?php } ?>		
											<?php //$i++; } ?>			
											</tbody>
										</table>
									
									</div>
									<!-- end widget content -->
				
								</div>
								<!-- end widget div -->
				
							</div>
							<!-- end widget -->
							
			
			<!--- Model POP UP - Approve user --->
		
			<div class="modal fade" id="myAcceptModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
            <div class="modal-dialog">
            <div class="modal-content">
            <div class="modal-header">
            <button type="button" class="close" data-dismiss="modal" aria-hidden="true"> &times; </button>
            <h4 class="modal-title" id="SuccessConfirmation">Confirmation</h4>
            </div>
            <div class="modal-body">
            <p>Are you sure want to approve ?</p>
            </div>
            <div class="modal-footer">
            <form action="<?php echo base_url();?>rates/approveHomeRate" name="RateHomeformAction" id="RateHomeformAction" method="post">
            <input type="hidden" name="approveHomeCountryCode" id="approveHomeCountryCode"  value="" />            
			<input type="submit" name="approveButton" id="approveButton" value="OK" class="btn btn-success rate_action_submit">
            <!--button type="button" class="btn btn-success rate_action_submit" id="rate_action_submit" name="rate_action_submit" > OK </button-->
            <button type="button" class="btn btn-danger" data-dismiss="modal" > Cancel </button>
			</form>
            </div>
            </div>
            </div>
			</div>
            <!-- /.modal-content --> 
			
			
			<!--- Model POP UP - Approve user --->
		
			<div class="modal fade" id="myRejectModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
            <div class="modal-dialog">
            <div class="modal-content">
            <div class="modal-header">
            <button type="button" class="close" data-dismiss="modal" aria-hidden="true"> &times; </button>
            <h4 class="modal-title" id="SuccessConfirmation">Confirmation</h4>
            </div>
            <div class="modal-body">
            <p>Are you sure want to approve ?</p>
            </div>
            <div class="modal-footer">
            <form action="<?php echo base_url();?>rates/approveHomeRate" name="RateHomeformAction" id="RateHomeformAction" method="post">
            <input type="hidden" name="approveHomeCountryCode" id="approveHomeCountryCode"  value="" />            
			<input type="submit" name="approveButton" id="approveButton" value="OK" class="btn btn-success rate_action_submit">
            <!--button type="button" class="btn btn-success rate_action_submit" id="rate_action_submit" name="rate_action_submit" > OK </button-->
            <button type="button" class="btn btn-danger" data-dismiss="modal" > Cancel </button>
			</form>
            </div>
            </div>
            </div>
			</div>
            <!-- /.modal-content --> 
			

						</article>
						<!-- WIDGET END -->
				
					</div>
				
					<!-- end row -->
				
					<!-- end row -->
				
				</section>
				<!-- end widget grid -->

			</div>
			<!-- END MAIN CONTENT -->

		</div>
		<!-- END MAIN PANEL -->

  <script src="//code.jquery.com/ui/1.11.4/jquery-ui.js"></script>
  <script>
  $(function() {
    var spinner = $( ".spinner" ).spinner();
    $( "button" ).button();
  });
  
   
  $(document).ready(function() {
	//alert('hai');
	
	// validate the comment form when it is submitted
	
	 $.validator.addMethod("pwchecknumber", function (value) {
        return /\d/.test(value) // has a digit
    }, "Password must contain at least one number");
	
	 $.validator.addMethod("pwcheckspechars", function (value) {
        return /[!@#$%^&*()_=\[\]{};':"\\|,.<>\/?+-]/.test(value)
    }, "Password must contain at least one special character");

		// validate signup form on keyup and submit
		$("#addOperatorUserform").validate({
			rules: {
				fullName: "required",
				userName: "required",
				user_email: {
					required: true,
					email: true
				},
				operator: "required",	
				password: {
					required: true,
					minlength: 6,
					maxlength: 20,
					pwchecknumber: true,
					pwcheckspechars: true
				},
				
				//building_name: "required",
				//street: "required",
				//city: "required",
				/*houseNo: {
					required: true,
					number: true
				},				
				*/
				
			},
			messages: {
				fullName: "Please enter a full name",
				userName: "Please enter a user name",
				user_email: {
					required: "Please enter an email address",
					email: "Please enter valid email id."
				},
				operator: "Please select a operator",
				password: {
					required: "Please enter a password",
					minlength: "Password must contain at least 6 characters",
					maxlength: "Password no more than 20 characters",
				},
				
				//building_name: "Please select a house name",
				//street: "Please select a street name",
				//city: "Please select a town name",
				/*houseNo: {
					required: "Please enter a house number",
					number: "Please enter valid house number.",
				},				
				*/					
			}
		});	

		
		$("#editOperatorUserform").validate({
			rules: {
				editFullName: "required",
				editUserName: "required",
				editUser_email: {
					required: true,
					email: true
				},
				editOperator: "required",	
				editPassword: "required",
				
				//building_name: "required",
				//street: "required",
				//city: "required",
				/*houseNo: {
					required: true,
					number: true
				},				
				*/
				
			},
			messages: {
				editFullName: "Please enter a full name",
				editUserName: "Please enter a user name",
				editUser_email: {
					required: "Please enter an email address",
					email: "Please enter valid email id."
				},
				editOperator: "Please select a operator",
				editPassword: "Please enter a password",
				
				//building_name: "Please select a house name",
				//street: "Please select a street name",
				//city: "Please select a town name",
				/*houseNo: {
					required: "Please enter a house number",
					number: "Please enter valid house number.",
				},				
				*/					
			}
		});	 
			
  });
  
  
    $("#fullName").blur(function(){
			$("#fullName").valid();
 	});	
	$("#userName").blur(function(){
			$("#userName").valid();
 	});
	$("#user_email").blur(function(){
			$("#user_email").valid();
 	});		
	$("#operator").blur(function(){
			$("#operator").valid();
 	});
	$("#password").blur(function(){
			$("#password").valid();
 	});
	
	$("#addUserValid").click(function(){
		$("em").empty();
	});
	
	
	$("#editFullName").blur(function(){
			$("#editFullName").valid();
 	});	
	$("#editUserName").blur(function(){
			$("#editUserName").valid();
 	});
	$("#editUser_email").blur(function(){
			$("#editUser_email").valid();
 	});		
	$("#editOperator").blur(function(){
			$("#editOperator").valid();
 	});
	$("#editPassword").blur(function(){
			$("#editPassword").valid();
 	});
	
	function editOperatorUser(operatorUserId){
		//alert(operatorUserId);
		$("em").empty();	
		var dataString = 'operatorUserId='+operatorUserId;		
		 $.ajax({
			   type: "POST",
			   url: "<?php echo base_url(); ?>admin/user/getOperatorUserById",
			   data: dataString,
			   dataType: 'json',
			   success: function(data){ 
					//alert(data);
					//alert(data.OperatorId);
					$("#editOperatorUserId").val(data.id);
					$("#editFullName").val(data.fullname);
					$("#editUserName").val(data.username);
					$("#editUser_email").val(data.email);
					$("#editOperator").val(data.operator_id);					
					$("#editPassword").val(data.password);
					$("#editStatus").val(data.status);
					//alert(data.status);
					if(data.status=='' || data.status=='0'){
						$("#status").show();
					}else{
						$("#status").hide();
					}		
			   }
			 }); 
	}
	
  function deleteOperatorUser(operatorUserId) {
   
  $.SmartMessageBox({
    title: "Confirmation alert?",
    content: "Are you want to delete this User?",
    buttons: '[No][Yes]'
  },
  function(ButtonPressed) {
    if (ButtonPressed === "Yes") {

     window.location.href = "<?php echo base_url(); ?>admin/user/deleteOperatorUserById/"+operatorUserId;
    }
   
  });
  operatorUserId.preventDefault();
}
	
	  
</script>