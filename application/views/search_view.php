		<!-- MAIN PANEL -->
		<div id="main" role="main">

		<!-- MAIN CONTENT -->
			<div id="content">

				<div class="row">
					<div class="col-xs-12 col-sm-7 col-md-7 col-lg-4">
						<h3 class="page-title txt-color-blueDark"><i class="fa-fw fa fa-search"></i> Search by Used Numbers </h3><!-- <span>&nbsp;>&nbsp; Device List</span> -->
					</div> 
				</div><!---->
				<!-- widget grid -->
				<section id="widget-grid" class="">
				
					<!-- row -->
					<div class="row">
				
						<!-- NEW WIDGET START -->
						<article class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
				
							<!-- Widget ID (each widget will need unique ID)-->
							
							<!-- end widget -->
				
							<!-- Widget ID (each widget will need unique ID)-->
							<button type="button" class="btn btn-default add"  data-toggle="modal" data-target="#myModal">Add User</button>	

							<!-- Widget ID (each widget will need unique ID)-->
							<div class="jarviswidget jarviswidget-color-blueDark m-t-20" id="wid-id-1" data-widget-editbutton="false" data-widget-editbutton="false"  data-widget-togglebutton="false" data-widget-editbutton="false" data-widget-colorbutton="false" data-widget-deletebutton="false" data-widget-fullscreenbutton="false" >
								
								<header>
									<!-- <span class="widget-icon"> <i class="fa fa-table"></i> </span> -->
									<h2>Search </h2>
				
								</header>
				
								<!-- widget div-->
								<div>
				
									<!-- widget edit box -->
								
									<!-- end widget edit box -->
									<!-- widget content -->
									<div class="widget-body no-padding">
										<div class="col-sm-12 col-md-12 prod-form">
											<div class="row">
												<form class="form-horizontal" method="post" action="">
													<div class="col-sm-12 col-md-12">
														<div class="form-group">
															<label class="col-sm-3  col-md-3 text-left">Search By<span class="pull-right line-height32">:</span></label>
															<div class="col-sm-3">
																<select class="form-control">
																	<option>UK</option>
																	<option>AT</option>
																	<option>SE</option>
																	<option>PT</option>
																</select>	
															</div>
															<div class="col-sm-3 Mm-t-20">
																
																<input class="form-control" type="text" name="searchValue" id="searchValue" value="" maxlength="25" autocomplete="off" placeholder="Enter value here" />
															</div>
															<div class="col-sm-3 Mm-t-20">
																<button type="submit" class="btn btn-primary">Search</button>
															</div>
														</div>														
													</div>													
												</form>
											</div>
										</div>
										
									</div>
								</div>
							</div>
							
							<div class="jarviswidget jarviswidget-color-blueDark" id="wid-id-1" data-widget-editbutton="false" data-widget-editbutton="false"  data-widget-togglebutton="false" data-widget-editbutton="false" data-widget-colorbutton="false" data-widget-deletebutton="false" data-widget-fullscreenbutton="false" >
								
								<header>
									<span class="widget-icon"> <i class="fa fa-table"></i> </span>
									<h2>User List</h2>				
								</header>
				
								<!-- widget div-->
								<div>
				
									<!-- widget edit box -->
								
									<!-- end widget edit box -->
				
									<!-- widget content -->
									<div class="widget-body no-padding">
										
										<table id="datatable_tabletools" class="table table-striped table-bordered table-hover" width="100%">
											<thead>
												<tr>
													<th data-class="expand">Account ID</th>
													<th data-hide="phone">Subscription ID</th>
													<th>Mundio Number</th>
													<th data-hide="phone">Etisalat Number</th>
													<th data-hide="phone,phoneL,tablet,tabletL,desktop">Date of Allocation</th>
													<th data-hide="phone,phoneL">Status</th>
													<th data-hide="phone,phoneL,tablet,tabletL,desktop,desktopL">Date of Activation</th>
													<th data-hide="phone,phoneL,tablet,tabletL,desktop,desktopL">Reason if Rejected</th>
													<th data-hide="phone,phoneL,tablet,tabletL,desktop,desktopL">Customer Name</th>
													<th data-hide="phone,phoneL,tablet,tabletL,desktop,desktopL,desktopXL">Address Details</th>	
													<th data-hide="phone,phoneL,tablet,tabletL,desktopXL">Alternate Phone Number</th>
													<th data-hide="phone,phoneL,tablet,tabletL">SIM Collected</th>
													<th data-hide="phone,phoneL,tablet,tabletL">IP Address</th>
													<th data-hide="phone,phoneL,tablet,desktop">Login Name</th>
													<th data-hide="phone,tablet">Email</th>													
											    </tr>
											</thead>
											<tbody>
											<?php //$i=1; foreach($arrOperatorUserList as $arrResult){  ?>
											<?php for($i=25;$i>=0;$i--){  ?>
												<tr>
													<td>1</td>
													<td>test</td>
													<td>test</td>
													<td>test</td>
													<td>test</td>
													<td>test</td>
													<td>test</td>													
												 	<td>test</td>
													<td>test</td>
													<td>test</td>
													<td>test</td>
													<td>test</td>
													<td>test</td>
													<td>test</td>
													<td>test</td>													
												</tr>	
											<?php } ?>		
											<?php //$i++; } ?>			
											</tbody>
										</table>
									
									</div>
									<!-- end widget content -->
				
								</div>
								<!-- end widget div -->
				
							</div>
							<!-- end widget -->
							
			
			<!-- modal-->
				
						

	<!-- modal end-->
			

						</article>
						<!-- WIDGET END -->
				
					</div>
				
					<!-- end row -->
				
					<!-- end row -->
				
				</section>
				<!-- end widget grid -->

			</div>
			<!-- END MAIN CONTENT -->

		</div>
		<!-- END MAIN PANEL -->

  <script src="//code.jquery.com/ui/1.11.4/jquery-ui.js"></script>
  <script>
  $(function() {
    var spinner = $( ".spinner" ).spinner();
    $( "button" ).button();
  });
  
   
  $(document).ready(function() {
	//alert('hai');
	
	// validate the comment form when it is submitted
	
	 $.validator.addMethod("pwchecknumber", function (value) {
        return /\d/.test(value) // has a digit
    }, "Password must contain at least one number");
	
	 $.validator.addMethod("pwcheckspechars", function (value) {
        return /[!@#$%^&*()_=\[\]{};':"\\|,.<>\/?+-]/.test(value)
    }, "Password must contain at least one special character");

		// validate signup form on keyup and submit
		$("#addOperatorUserform").validate({
			rules: {
				fullName: "required",
				userName: "required",
				user_email: {
					required: true,
					email: true
				},
				operator: "required",	
				password: {
					required: true,
					minlength: 6,
					maxlength: 20,
					pwchecknumber: true,
					pwcheckspechars: true
				},
				
				//building_name: "required",
				//street: "required",
				//city: "required",
				/*houseNo: {
					required: true,
					number: true
				},				
				*/
				
			},
			messages: {
				fullName: "Please enter a full name",
				userName: "Please enter a user name",
				user_email: {
					required: "Please enter an email address",
					email: "Please enter valid email id."
				},
				operator: "Please select a operator",
				password: {
					required: "Please enter a password",
					minlength: "Password must contain at least 6 characters",
					maxlength: "Password no more than 20 characters",
				},
				
				//building_name: "Please select a house name",
				//street: "Please select a street name",
				//city: "Please select a town name",
				/*houseNo: {
					required: "Please enter a house number",
					number: "Please enter valid house number.",
				},				
				*/					
			}
		});	

		
		$("#editOperatorUserform").validate({
			rules: {
				editFullName: "required",
				editUserName: "required",
				editUser_email: {
					required: true,
					email: true
				},
				editOperator: "required",	
				editPassword: "required",
				
				//building_name: "required",
				//street: "required",
				//city: "required",
				/*houseNo: {
					required: true,
					number: true
				},				
				*/
				
			},
			messages: {
				editFullName: "Please enter a full name",
				editUserName: "Please enter a user name",
				editUser_email: {
					required: "Please enter an email address",
					email: "Please enter valid email id."
				},
				editOperator: "Please select a operator",
				editPassword: "Please enter a password",
				
				//building_name: "Please select a house name",
				//street: "Please select a street name",
				//city: "Please select a town name",
				/*houseNo: {
					required: "Please enter a house number",
					number: "Please enter valid house number.",
				},				
				*/					
			}
		});	 
			
  });
  
  
    $("#fullName").blur(function(){
			$("#fullName").valid();
 	});	
	$("#userName").blur(function(){
			$("#userName").valid();
 	});
	$("#user_email").blur(function(){
			$("#user_email").valid();
 	});		
	$("#operator").blur(function(){
			$("#operator").valid();
 	});
	$("#password").blur(function(){
			$("#password").valid();
 	});
	
	$("#addUserValid").click(function(){
		$("em").empty();
	});
	
	
	$("#editFullName").blur(function(){
			$("#editFullName").valid();
 	});	
	$("#editUserName").blur(function(){
			$("#editUserName").valid();
 	});
	$("#editUser_email").blur(function(){
			$("#editUser_email").valid();
 	});		
	$("#editOperator").blur(function(){
			$("#editOperator").valid();
 	});
	$("#editPassword").blur(function(){
			$("#editPassword").valid();
 	});
	
	function editOperatorUser(operatorUserId){
		//alert(operatorUserId);
		$("em").empty();	
		var dataString = 'operatorUserId='+operatorUserId;		
		 $.ajax({
			   type: "POST",
			   url: "<?php echo base_url(); ?>admin/user/getOperatorUserById",
			   data: dataString,
			   dataType: 'json',
			   success: function(data){ 
					//alert(data);
					//alert(data.OperatorId);
					$("#editOperatorUserId").val(data.id);
					$("#editFullName").val(data.fullname);
					$("#editUserName").val(data.username);
					$("#editUser_email").val(data.email);
					$("#editOperator").val(data.operator_id);					
					$("#editPassword").val(data.password);
					$("#editStatus").val(data.status);
					//alert(data.status);
					if(data.status=='' || data.status=='0'){
						$("#status").show();
					}else{
						$("#status").hide();
					}		
			   }
			 }); 
	}
	
  function deleteOperatorUser(operatorUserId) {
   
  $.SmartMessageBox({
    title: "Confirmation alert?",
    content: "Are you want to delete this User?",
    buttons: '[No][Yes]'
  },
  function(ButtonPressed) {
    if (ButtonPressed === "Yes") {

     window.location.href = "<?php echo base_url(); ?>admin/user/deleteOperatorUserById/"+operatorUserId;
    }
   
  });
  operatorUserId.preventDefault();
}
	
	  
</script>