		<!-- MAIN PANEL -->
		<div id="main" role="main">

		<!-- MAIN CONTENT -->
			<div id="content">

				<div class="row">
					<div class="col-xs-12 col-sm-7 col-md-7 col-lg-4">
						<h3 class="page-title txt-color-blueDark"><i class="fa-fw fa fa-user"></i> Users </h3><!-- <span>&nbsp;>&nbsp; Device List</span> -->
					</div> 
				</div>
				<!-- widget grid -->
				<section id="widget-grid" class="">
				
					<!-- row -->
					<div class="row">
				
						<!-- NEW WIDGET START -->
						<article class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
				
							<!-- Widget ID (each widget will need unique ID)-->
							
							<!-- end widget -->
				
							<!-- Widget ID (each widget will need unique ID)-->
							<button type="button" class="btn btn-default add"  data-toggle="modal" data-target="#myModal">Add User</button>					
							<div class="jarviswidget jarviswidget-color-blueDark m-t-20" id="wid-id-1" data-widget-editbutton="false" data-widget-editbutton="false"  data-widget-togglebutton="false" data-widget-editbutton="false" data-widget-colorbutton="false" data-widget-deletebutton="false" data-widget-fullscreenbutton="false" >
								
								<header>
									<span class="widget-icon"> <i class="fa fa-table"></i> </span>
									<h2>User List</h2>				
								</header>
				
								<!-- widget div-->
								<div>
				
									<!-- widget edit box -->
								
									<!-- end widget edit box -->
				
									<!-- widget content -->
									<div class="widget-body no-padding">
										
										<table id="datatable_tabletools" class="table table-striped table-bordered table-hover" width="100%">
											<thead>
												<tr>
													<th data-hide="phone">ID</th>
													<th data-class="expand">Full Name</th>
													<th>UserName</th>
													<th data-hide="phone">Email</th>
													<th data-hide="phone,tablet">Operator</th>
													<th data-hide="phone,tablet">Last Login</th>
													<th data-hide="phone,tablet">IP Address Role</th>
													<th data-hide="phone,tablet">Status</th>
													<th data-hide="phone,tablet">Action</th>				
											    </tr>
											</thead>
											<tbody>
											<?php $i=1; foreach($arrOperatorUserList as $arrResult){  ?>
												<tr>
													<td><?php echo $i; ?></td>
													<td><?php echo $arrResult['fullname']; ?></td>
													<td><?php echo $arrResult['username']; ?></td>
													<td><?php echo $arrResult['email']; ?></td>
													<td><?php echo $arrResult['operatorname']; ?></td>
													<td><?php echo $arrResult['lastlogin']; ?></td>
													<td><?php echo $arrResult['ipaddress']; ?></td>													
												 	<td><?php if($arrResult['status'] == '1'){ echo 'Active'; }else{ echo 'Inactive'; } ?></td>
													<td>
													<?php //if($arrResult['status'] == '1'){ ?>
													<a href="#" onclick="editOperatorUser('<?php echo $arrResult['id']; ?>');" data-toggle="modal" data-target="#myEditOperatorUserModal"><i class="fa fa-edit"></i> Edit</a>&nbsp;<?php if($arrResult['status'] == '1'){ ?>|&nbsp;<a href="#" onclick="deleteOperatorUser('<?php echo $arrResult['id']; ?>');"><i class="fa fa-times"></i> Delete</a><?php } ?> 
													<?php //} ?>
													</td>
												</tr>	
											<?php $i++; } ?>			
											</tbody>
										</table>
									
									</div>
									<!-- end widget content -->
				
								</div>
								<!-- end widget div -->
				
							</div>
							<!-- end widget -->
							
			
			<!-- modal-->
				 <div class="modal fade" id="myModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
					<div class="modal-dialog">
						<div class="modal-content">
							<div class="modal-header">
								<button type="button" class="close" data-dismiss="modal" aria-hidden="true">
									&times;
								</button>
								<h4 class="modal-title" id="myModalLabel">New User Form</h4>
							</div>
							<div class="modal-body">
							<form name="addOperatorUserform"  id="addOperatorUserform" class="form-horizontal" method="post" action="<?php echo base_url();?>admin/user/addOperatorUser">	
								
									
								   <div class="form-group">
										<label for="fullName" class="control-label col-md-3" >Full Name</label>
										<div class="col-md-6">
											<input type="text" class="form-control" id="fullName" name="fullName" placeholder="Full Name" maxlength="25" autocomplete="off" />
										</div>
									</div>									
								
															
									
									<div class="form-group">
									<label for="userName" class="col-md-3 control-label">User name</label>
										<div class="col-md-6">
											<input type="text" class="form-control" name="userName" id="userName" placeholder="User Name" maxlength="25" autocomplete="off" />
										</div>
									</div>
								
								
								
										<div class="form-group">											
											<label for="user_email"  class="col-md-3 control-label" >Email Address</label>
										<div class="col-md-6">
											<input type="text" class="form-control" name="user_email" id="user_email" placeholder="Enter email address" maxlength="40" autocomplete="off" />
										</div>
									</div>
								
							
								
										<div class="form-group">												
											<label for="operator"  class="col-md-3 control-label" >Operator</label>
											<div class="col-md-6">
												<select class="form-control" id="operator" name="operator">
													<option value="">Select Operator</option>
													<?php  foreach($arrActiveOperatorList as $key=>$value){ ?>
													<option  value="<?php echo $value['id']; ?>"><?php echo $value['operatorname']; ?></option>      
													<?php } ?>	
												</select>
											</div>
										</div>
																						
							
									
									<div class="form-group">
										<label for="password"  class="col-md-3 control-label" >Password</label>
										<div class="col-md-6">
											<input type="password" class="form-control" name="password" id="password" placeholder="Enter Password"  maxlength="25" autocomplete="off" />
										</div>
									</div>
								
				
							</div>
							<div class="modal-footer text-center">
								<!--button type="button" class="btn btn-default" >
									Ok
								</button-->
								<input type="submit" name="addOperatorUser" id="addOperatorUser" value="Ok" class="btn btn-default">
								
								<button type="button" class="btn btn-primary" data-dismiss="modal">
									cancel
								</button>
							</div>
							</form>
						</div><!-- /.modal-content -->
					</div><!-- /.modal-dialog -->
				</div>
						

	<!-- modal end-->


	<!-- modal edit user form-->
				 <div class="modal fade" id="myEditOperatorUserModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
					<div class="modal-dialog">
						<div class="modal-content">
							<div class="modal-header">
								<button type="button" class="close" data-dismiss="modal" aria-hidden="true">
									&times;
								</button>
								<h4 class="modal-title" id="myModalLabel">Edit User Form</h4>
							</div>
							
							<div class="modal-body">
								<form name="editOperatorUserform" class="form-horizontal" id="editOperatorUserform" method="post" action="<?php echo base_url();?>admin/user/updateOperatorUser">	
								<input type="hidden" name="editOperatorUserId" id="editOperatorUserId" value="" />
								<input type="hidden" name="editPassword" id="editPassword" value="" />
								
								  <div class="form-group">
										<label for="editFullName" class="control-label col-md-3">Full Name</label>
											<div class="col-md-6">
											<input type="text" class="form-control" id="editFullName" name="editFullName" placeholder="Full Name" maxlength="25" autocomplete="off" />
										</div>
								  </div>									
							
																
								    <div class="form-group">
											<label for="editUserName" class="control-label col-md-3">User name</label>
										<div class="col-md-6">
											<input type="text" class="form-control" name="editUserName" id="editUserName" placeholder="User Name" maxlength="25" autocomplete="off" />
										</div>
									</div>
							
								
								   <div class="form-group">											
											<label for="editUser_email" class="control-label col-md-3">Email Address</label>
										<div class="col-md-6">
											<input type="text" class="form-control" name="editUser_email" id="editUser_email" placeholder="Enter email address" maxlength="40" autocomplete="off" />
										</div>
									</div>
								
					
								<div class="form-group">												
										<label for="editOperator" class="control-label col-md-3">Operator</label>
										<div class="col-md-6">
											<select class="form-control" id="editOperator" name="editOperator">
													<option value="">Select Operator</option>
													<?php  foreach($arrActiveOperatorList as $key=>$value){ ?>
												<option  value="<?php echo $value['id']; ?>"><?php echo $value['operatorname']; ?></option>      
													<?php } ?>	
											</select>
										</div>
								</div>															
								<!--div class="row" style="display: none">
									<div class="col-md-6">
										<div class="form-group">
											<label for="editPassword">Password</label>
											<input type="password" class="form-control" name="editPassword" id="editPassword" placeholder="Enter Password" maxlength="25" autocomplete="off" />
										</div>
									</div>
								</div-->
							
									
										<div class="form-group"  id="status" style="display: none">
											<label for="editStatus" class="control-label col-md-3">Status</label>
												<div class="col-md-6">	
													<select class="form-control" id="editStatus" name="editStatus">
														<option value="">Select Status</option>
														<option value="0">Inactive</option>
														<option value="1">Active</option>
													</select>
											   </div>
										</div>
							
				
							</div>
							<div class="modal-footer text-center">
								<!--button type="button" class="btn btn-default" >
									Ok
								</button-->
								<input type="submit" name="editOperatorUser" id="editOperatorUser" value="Ok" class="btn btn-default">
								
								<button type="button" class="btn btn-primary" data-dismiss="modal">
									cancel
								</button>
							</div>
							</form>
						</div><!-- /.modal-content -->
					</div><!-- /.modal-dialog -->
				</div>
						

	<!-- modal end-->
			

						</article>
						<!-- WIDGET END -->
				
					</div>
				
					<!-- end row -->
				
					<!-- end row -->
				
				</section>
				<!-- end widget grid -->

			</div>
			<!-- END MAIN CONTENT -->

		</div>
		<!-- END MAIN PANEL -->

  <script src="//code.jquery.com/ui/1.11.4/jquery-ui.js"></script>
  <script>
  $(function() {
    var spinner = $( ".spinner" ).spinner();
    $( "button" ).button();
  });
  
   
  $(document).ready(function() {
	//alert('hai');
	
	// validate the comment form when it is submitted
	
	 $.validator.addMethod("pwchecknumber", function (value) {
        return /\d/.test(value) // has a digit
    }, "Password must contain at least one number");
	
	 $.validator.addMethod("pwcheckspechars", function (value) {
        return /[!@#$%^&*()_=\[\]{};':"\\|,.<>\/?+-]/.test(value)
    }, "Password must contain at least one special character");

		// validate signup form on keyup and submit
		$("#addOperatorUserform").validate({
			rules: {
				fullName: "required",
				userName: "required",
				user_email: {
					required: true,
					email: true
				},
				operator: "required",	
				password: {
					required: true,
					minlength: 6,
					maxlength: 20,
					pwchecknumber: true,
					pwcheckspechars: true
				},
				
				//building_name: "required",
				//street: "required",
				//city: "required",
				/*houseNo: {
					required: true,
					number: true
				},				
				*/
				
			},
			messages: {
				fullName: "Please enter a full name",
				userName: "Please enter a user name",
				user_email: {
					required: "Please enter an email address",
					email: "Please enter valid email id."
				},
				operator: "Please select a operator",
				password: {
					required: "Please enter a password",
					minlength: "Password must contain at least 6 characters",
					maxlength: "Password no more than 20 characters",
				},
				
				//building_name: "Please select a house name",
				//street: "Please select a street name",
				//city: "Please select a town name",
				/*houseNo: {
					required: "Please enter a house number",
					number: "Please enter valid house number.",
				},				
				*/					
			}
		});	

		
		$("#editOperatorUserform").validate({
			rules: {
				editFullName: "required",
				editUserName: "required",
				editUser_email: {
					required: true,
					email: true
				},
				editOperator: "required",	
				editPassword: "required",
				
				//building_name: "required",
				//street: "required",
				//city: "required",
				/*houseNo: {
					required: true,
					number: true
				},				
				*/
				
			},
			messages: {
				editFullName: "Please enter a full name",
				editUserName: "Please enter a user name",
				editUser_email: {
					required: "Please enter an email address",
					email: "Please enter valid email id."
				},
				editOperator: "Please select a operator",
				editPassword: "Please enter a password",
				
				//building_name: "Please select a house name",
				//street: "Please select a street name",
				//city: "Please select a town name",
				/*houseNo: {
					required: "Please enter a house number",
					number: "Please enter valid house number.",
				},				
				*/					
			}
		});	 
			
  });
  
  
    $("#fullName").blur(function(){
			$("#fullName").valid();
 	});	
	$("#userName").blur(function(){
			$("#userName").valid();
 	});
	$("#user_email").blur(function(){
			$("#user_email").valid();
 	});		
	$("#operator").blur(function(){
			$("#operator").valid();
 	});
	$("#password").blur(function(){
			$("#password").valid();
 	});
	
	$("#addUserValid").click(function(){
		$("em").empty();
	});
	
	
	$("#editFullName").blur(function(){
			$("#editFullName").valid();
 	});	
	$("#editUserName").blur(function(){
			$("#editUserName").valid();
 	});
	$("#editUser_email").blur(function(){
			$("#editUser_email").valid();
 	});		
	$("#editOperator").blur(function(){
			$("#editOperator").valid();
 	});
	$("#editPassword").blur(function(){
			$("#editPassword").valid();
 	});
	
	function editOperatorUser(operatorUserId){
		//alert(operatorUserId);
		$("em").empty();	
		var dataString = 'operatorUserId='+operatorUserId;		
		 $.ajax({
			   type: "POST",
			   url: "<?php echo base_url(); ?>admin/user/getOperatorUserById",
			   data: dataString,
			   dataType: 'json',
			   success: function(data){ 
					//alert(data);
					//alert(data.OperatorId);
					$("#editOperatorUserId").val(data.id);
					$("#editFullName").val(data.fullname);
					$("#editUserName").val(data.username);
					$("#editUser_email").val(data.email);
					$("#editOperator").val(data.operator_id);					
					$("#editPassword").val(data.password);
					$("#editStatus").val(data.status);
					//alert(data.status);
					if(data.status=='' || data.status=='0'){
						$("#status").show();
					}else{
						$("#status").hide();
					}		
			   }
			 }); 
	}
	
  function deleteOperatorUser(operatorUserId) {
   
  $.SmartMessageBox({
    title: "Confirmation alert?",
    content: "Are you want to delete this User?",
    buttons: '[No][Yes]'
  },
  function(ButtonPressed) {
    if (ButtonPressed === "Yes") {

     window.location.href = "<?php echo base_url(); ?>admin/user/deleteOperatorUserById/"+operatorUserId;
    }
   
  });
  operatorUserId.preventDefault();
}
	
	  
</script>