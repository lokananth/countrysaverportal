		<!-- MAIN PANEL -->
		<div id="main" role="main">
	<!-- MAIN CONTENT -->
			<div id="content">

				<!-- row -->
				<div class="row"> 
					<div class="col-sm-offset-3 col-sm-6 col-xs-12">
					 <div id="content" class=" full-page login">
				  
							<form method="post" action="<?php echo base_url();?>login/checkAdminLogin" id="login-form" class="smart-form client-form">
							 <img src="<?php echo base_url(); ?>assets/img/logo-mundio1.png" alt class="logo"> 
								<header>
									<b>Etisalat Nigeria Bundle Management Portal</b>
								</header>
								<?php if($this->session->flashdata('errmsg')!=''){ ?>
								<div class="alert alert-danger" id="errmsg" >
									<?php echo $this->session->flashdata('errmsg'); ?>
								</div>
								<?php } ?>
								<fieldset> 
									<section>
										<label class="label">Username</label>
										<label class="input"> <i class="icon-append fa fa-user"></i>
											<input type="text" id="username" name="username" value="" required="required" maxlength="25" autocomplete="off" />
											<b class="tooltip tooltip-top-right"><i class="fa fa-user txt-color-teal"></i> Please enter username</b></label>
									</section>
									<section>
										<label class="label">Email</label>
										<label class="input"> <i class="icon-append fa fa-envelope-o"></i>
											<input type="email" id="email" name="email" value="" required="required" maxlength="50" autocomplete="off" />
											<b class="tooltip tooltip-top-right"><i class="fa fa-envelope-o txt-color-teal"></i> Please enter email</b></label>
									</section>
									<section>
										<label class="label">Password</label>
										<label class="input"> <i class="icon-append fa fa-lock"></i>
											<input type="password" id="password" name="password" required="required" maxlength="25" autocomplete="off"/>
											<b class="tooltip tooltip-top-right"><i class="fa fa-lock txt-color-teal"></i> Enter your password</b> </label>
										<!-- <div class="note">
											<a href="forgotpassword.html">Forgot password?</a>
										</div> -->
									</section> 
									<div>
										<label><input type="checkbox" value="remember-me" id="remember_me"> Remember me</label>
									 </div>
								</fieldset>
								<footer>
									<button type="submit" class="btn btn-primary">
										Sign in
									</button>
								</footer>
							</form>

						</div>
							 
				
						</div>
				
					</div>

				<!-- end row -->

			</div>
			
			</div>
			<!-- END MAIN CONTENT -->

		</div>
		<!-- END MAIN PANEL -->
<script>
 
  $(document).ready(function() {
	 //alert('hai');
	 $("#username").focus(function(){
		$("#errmsg").hide();
	 });
	 $("#email").focus(function(){
		$("#errmsg").hide();
	 });
	 $("#password").focus(function(){
		$("#errmsg").hide();
	 });
	 
	
	 if (localStorage.chkbx && localStorage.chkbx != '') {
			$('#remember_me').attr('checked', 'checked');
			$('#username').val(localStorage.usrname);
			$('#email').val(localStorage.email);
			$('#password').val(localStorage.password);
		} else {
			/*$('#remember_me').removeAttr('checked');
			$('#username').val('');
			$('#email').val('');
			$('#password').val('');*/
		}

		$('#remember_me').click(function() {

			if ($('#remember_me').is(':checked')) {
				// save username and password
				localStorage.usrname = $('#username').val();
				localStorage.email = $('#email').val();
				localStorage.password = $('#password').val();
				localStorage.chkbx = $('#remember_me').val();
			} else {
				localStorage.usrname = '';
				localStorage.email = '';
				localStorage.password = '';
				localStorage.chkbx = '';
			}
		});
	 
  });
	  
</script>	 		