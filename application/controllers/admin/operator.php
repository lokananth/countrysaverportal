<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Operator extends CI_Controller {	
	 
	function __Construct(){
		parent::__Construct ();
		session_start();	
		if($_SESSION['username']==''){
			redirect('admin/login');
		}
		$this->load->library('session');	
	}
	
	public function index()
	{
		//$this->load->library('session');
		$data = array();
		$data['arrOperatorList'] = ApiPostHeader($this->config->item('GetOperatorInfo'), '');
		$data['arrCountryList'] = ApiPostHeader($this->config->item('GetCountryDetails'), '');
		//echo '<pre>';print_r($data);exit;
		
		$this->load->view('admin/header_view');
		$this->load->view('admin/operator_view',$data);
		$this->load->view('admin/footer_view');
	}

	public function addOperator(){
		//echo '<pre>';print_r($_REQUEST);exit;
		//$this->load->library('session');
		if(isset($_REQUEST['operatorName'])){
			$varOperatorName = trim($_REQUEST['operatorName']);
			$arrCountry = explode('~',$_REQUEST['country']);
			$varCountryCode = $arrCountry[0];
			$varCountry = $arrCountry[1];
			$varContactName = trim($_REQUEST['contactName']);
			$varEmail = trim($_REQUEST['oper_email']);
			
			$params = array('OperatorName'=>$varOperatorName,'Countrycode'=>$varCountryCode,'CountryName'=>$varCountry,'ContactName'=>$varContactName,'Email'=>$varEmail,'createdBy'=>$_SESSION['username']);
			$arrAddOperator = ApiPostHeader($this->config->item('InsertOperatorInfo'), $params);
			//echo '<pre>';print_r($params);print_r($arrAddOperator);exit;
			if($arrAddOperator['errcode']=='0'){
				$this->session->set_flashdata('successmsg','Operator added successfully');
			}else{
				$this->session->set_flashdata('successmsg','Please try again later');
			}
			redirect('admin/operator');
		}else{
			redirect('admin/operator');
		}		
	}
		
	public function getOperatorById(){
		$varOperatorId =  $this->input->post('operatorId');
		if($varOperatorId!=''){
			$params = array('OperatorId'=>$varOperatorId);
			$arrGetOperatorById = ApiPostHeader($this->config->item('GetOperatorInfoById'), $params);
			//echo '<pre>';print_r($params);print_r($arrGetOperatorById);exit;
			if($arrGetOperatorById['errcode']=='0'){
				echo json_encode($arrGetOperatorById);	
			}else{
				echo '';
			}	
		}else{
			echo '';
		}		
	}
	
	public function updateOperator(){
		//echo '<pre>';print_r($_REQUEST);exit;
		if(isset($_REQUEST['editOperatorId'])){
			$varOperatorId = trim($_REQUEST['editOperatorId']);
			$varOperatorName = trim($_REQUEST['editOperatorName']);
			$arrCountry = explode('~',$_REQUEST['editCountry']);
			$varCountryCode = $arrCountry[0];
			$varCountry = $arrCountry[1];
			$varContactName = trim($_REQUEST['editContactName']);
			$varEmail = trim($_REQUEST['editOper_email']);
			$varStatus = trim($_REQUEST['editStatus']);
			
			$params = array('OperatorId'=>$varOperatorId,'OperatorName'=>$varOperatorName,'Countrycode'=>$varCountryCode,'CountryName'=>$varCountry,'ContactName'=>$varContactName,'Email'=>$varEmail,'updated_by'=>$_SESSION['username'],'Status'=>$varStatus);
			$arrUpdateOperator = ApiPostHeader($this->config->item('UpdateOperatorInfo'), $params);
			//echo '<pre>';print_r($params);print_r($arrUpdateOperator);exit;
			if($arrUpdateOperator['errcode']=='0'){
				$this->session->set_flashdata('successmsg','Operator updated successfully');
			}else{
				$this->session->set_flashdata('successmsg','Please try again later');
			}
			redirect('admin/operator');
		}else{
			redirect('admin/operator');
		}		
	}
	
	public function deleteOperatorById($varOperatorId){		
		if($varOperatorId!=''){
			$params = array('OperatorId'=>$varOperatorId,'Status'=>'0','deleted_by'=>$_SESSION['username']);
			$arrDeleteOperatorById = ApiPostHeader($this->config->item('UpdateOperatorStatusById'), $params);
			//echo '<pre>';print_r($params);print_r($arrDeleteOperatorById);exit;
			if($arrDeleteOperatorById['errcode']=='0'){
				$this->session->set_flashdata('successmsg','Operator deleted successfully');					
			}else{
				$this->session->set_flashdata('successmsg','Please try again later');
			}
			redirect('admin/operator');	
		}else{
			echo '';
		}		
	}
}

/* End of file welcome.php */
/* Location: ./application/controllers/welcome.php */