<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Login extends CI_Controller {
	
	public function index()
	{	
		$this->load->library('session');		
		$this->load->view('header_view');
		$this->load->view('login_view');
		$this->load->view('footer_view');
	}
		
	public function checkAdminLogin(){
		//echo 'hai';exit;
		//echo '<pre>';print_r($_REQUEST);exit;
		$this->load->library('session');
		
		$varUsername = trim($_REQUEST['username']);
		$varPassword = trim($_REQUEST['password']);		
		
		$params = array('username'=>$varUsername,'Password'=>$varPassword);
		$arrLoginInfo = ApiPostHeader($this->config->item('AdminLogin'), $params);
		//echo '<pre>';print_r($params);print_r($arrLoginInfo);exit;		
		$arrLoginInfo['errcode'] = '0';
		if($arrLoginInfo['errcode']=='0'){			
			session_start();
			$_SESSION['username'] = $varUsername;			
			redirect('search');					
		}else{
			//$this->session->set_flashdata('errmsg',$arrLoginInfo['errmsg']);
			$this->session->set_flashdata('errmsg','Please enter a valid username, email & password');
			redirect('login');
		}		
	}
	
	public function signOut(){
		session_start();
		session_destroy();
		session_unset();
		unset($_SESSION['username']);
		redirect('login');
	}	
}

/* End of file welcome.php */
/* Location: ./application/controllers/welcome.php */