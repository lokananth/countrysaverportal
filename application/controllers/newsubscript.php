<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Newsubscript extends CI_Controller {
	
	function __Construct(){
		parent::__Construct ();
		session_start();	
		if($_SESSION['username']==''){
			redirect('admin/login');
		}
		$this->load->library('session');	
	}
	
	public function index()
	{
		$this->load->library('session');
		//$data = array();
		//$data['arrOperatorUserList'] = ApiPostHeader($this->config->item('GetUserInfoDetails'), '');
		//$data['arrActiveOperatorList'] = ApiPostHeader($this->config->item('GetActiveOperatorList'), '');
		//echo '<pre>';print_r($data);exit;
		
		$this->load->view('header_view');
		$this->load->view('innerMenu_view');
		$this->load->view('leftMenu_view');
		$this->load->view('newSubscript_view');		
		$this->load->view('footer_view');
	}

	/*public function addOperatorUser(){
		//echo '<pre>';print_r($_REQUEST);exit;
		//$this->load->library('session');
		if(isset($_REQUEST['fullName'])){
			$varFullName = trim($_REQUEST['fullName']);
			$varUserName = trim($_REQUEST['userName']);
			$varEmail = trim($_REQUEST['user_email']);
			$varOperatorId = trim($_REQUEST['operator']);
			$varPassword = trim($_REQUEST['password']);	
			
			$params = array('fullName'=>$varFullName,'userName'=>$varUserName,'email'=>$varEmail,'operatorId'=>$varOperatorId,'password'=>$varPassword,'createdBy'=>$_SESSION['username']);
			$arrAddOperatorUser = ApiPostHeader($this->config->item('InsertOperatorUserInfo'), $params);
			//echo '<pre>';print_r($params);print_r($arrAddOperatorUser);exit;
			if($arrAddOperatorUser['errcode']=='0'){
				$this->session->set_flashdata('successmsg','User added successfully');
			}else{
				$this->session->set_flashdata('successmsg','Please try again later');
			}
			redirect('admin/user');
		}else{
			redirect('admin/user');
		}		
	}
		
	public function getOperatorUserById(){
		$varOperatorUserId =  $this->input->post('operatorUserId');
		if($varOperatorUserId!=''){
			$params = array('id'=>$varOperatorUserId);
			$arrGetOperatorUserById = ApiPostHeader($this->config->item('GetUserById'), $params);
			//echo '<pre>';print_r($params);print_r($arrGetOperatorUserById);exit;
			if($arrGetOperatorUserById['errcode']=='0'){
				echo json_encode($arrGetOperatorUserById);	
			}else{
				echo '';
			}	
		}else{
			echo '';
		}		
	}
	
	public function updateOperatorUser(){
		//echo '<pre>';print_r($_REQUEST);exit;
		if(isset($_REQUEST['editOperatorUserId'])){
			$varOperatorUserId = trim($_REQUEST['editOperatorUserId']);
			$varFullName = trim($_REQUEST['editFullName']);
			$varUserName = trim($_REQUEST['editUserName']);
			$varEmail = trim($_REQUEST['editUser_email']);
			$varOperatorId = trim($_REQUEST['editOperator']);
			$varPassword = trim($_REQUEST['editPassword']);
			$varStatus = trim($_REQUEST['editStatus']);	
			
			$params = array('UserId'=>$varOperatorUserId,'fullName'=>$varFullName,'userName'=>$varUserName,'email'=>$varEmail,'operatorId'=>$varOperatorId,'password'=>$varPassword,'updated_by'=>$_SESSION['username'],'Status'=>$varStatus);			
			$arrUpdateOperatorUser = ApiPostHeader($this->config->item('UpdateOperatorUserInfo'), $params);
			//echo '<pre>';print_r($params);print_r($arrUpdateOperatorUser);exit;
			if($arrUpdateOperatorUser['errcode']=='0'){
				$this->session->set_flashdata('successmsg','User updated successfully');
			}else{
				$this->session->set_flashdata('successmsg','Please try again later');
			}
			redirect('admin/user');
		}else{
			redirect('admin/user');
		}	
	}
	
	public function deleteOperatorUserById($varOperatorUserId){		
		if($varOperatorUserId!=''){
			$params = array('UserId'=>$varOperatorUserId,'Status'=>'0','deleted_by'=>$_SESSION['username']);
			$arrDeleteOperatorUserById = ApiPostHeader($this->config->item('UpdateOperatorUserInfoStatus'), $params);
			//echo '<pre>';print_r($params);print_r($arrDeleteOperatorUserById);exit;
			if($arrDeleteOperatorUserById['errcode']=='0'){
				$this->session->set_flashdata('successmsg','User deleted successfully');					
			}else{
				$this->session->set_flashdata('successmsg','Please try again later');
			}
			redirect('admin/user');	
		}else{
			echo '';
		}		
	}*/
}

/* End of file welcome.php */
/* Location: ./application/controllers/welcome.php */